# 1.5.0:
### Changes to existing code:
  - Updated method docs for conversion methods in AbstractString and JsonValue, JsonArray & JsonObject copy constructors, and some other methods.
  - ToJsonObject(IEnumerable<JsonToken>) extension method now does not accepts nulls.
  - ToJsonObject(IEnumerable<KeyValuePair<string,string>>) extension method now does not allow null keys.
  - ToJsonArray(IEnumerable<JsonObject> or IEnumerable<JsonArray>) extension methods now accept null items.
  - Serialization.TokensFromTypeReadable() method has been renamed to TokensReadable(). My bad, it havent even taken type argument.
  - AbstractString's ToInt32(), ToInt64() and ToSingle() methods have been renamed to their respective type keyword (ToInt(), ToLong() and ToFloat()), what matches with style that JsonToken/JsonValue use.
  - One of bool flags in JsonStream.StreamFile() (caching, IIRC), was removed. It made no further sense. Also current StreamFile's caching strategy is to always cache tokens, regardless of Include() being called or not. For scenarios involving using IToken out of enumeration use DrawTokens() instead.
  - IToken.Include() will now throw InvalidOperationException when called after enumerator has been disposed. Usually rewording of linq methods helps resolve this issue.
  - Both JsonStream's StreamFile() and StreamConverted() will throw InvalidOperationException when being enumerated again with captured enumerable instance.
  - Usage of IToken.QueryTokens() for json object is discouraged. Use indexer instead (it's also a lot more sensible).
  - IToken.Include() got bool argument with default value = true, which doesn't alter current behaviour in any way.
  
### Fixes:
  - GetHashCode() would throw exceptions in certain cases.
  - AbstractString[int] wouldn't throw IndexOutOfRangeException in some cases.
  - AbstractString.ToInt64() and ToInt32() wouldn't parse consistently with JsonValue.IntStyle.
  - When parsed with Json.ValueCorrectnessChecks flag set, incorrect and quoted value would pass validation in some cases. Also string detection via detectString args would incorrectly behave in such cases.
  - JsonToken and JsonValue SetValue() methods would sometimes misbehave in terms of quotes and null replacement. Currently null string is converted to runtime null only if it was written without quotes, whereas quoted or "" should remain unchanged.
  - JsonToken or JsonValue, produced by IToken.Convert(), Value or ToString() would in rare cases violate said rules.
  - Both JsonStream.StreamFile() and StreamConverted() wouldn't clear cache when enumeration wasn't full.
  - Parsing from StreamReader in some cases would cause stream retention that prevented garbage collection. That issue has been resolved for fully parsed source. In other case Dispose() static method should be called. This issue should be addressed in future releases.
  - JsonStream.StreamFile() would behave unpredictable in some cases.
  - IToken.Include() wouln't cause parsing to happen in some cases.
  
### New features:
  - IJsonNameStyle interface, that you may implement to easily convert member names to any custom format. Pass an instance to Deserialization' or Serialization' NameStyle property. [JsonName] has a precedence over style, so backward compatibility with older model definitions is maintained. Member names are computed only once for given type. Also constructor parameter names remain untouched.
  - Serialization basic support for writing JsonComposite that is in object type field or property as part of regular model. This is complementary functionality to deserialization of undefined type.
  - JsonToken.ToTokenDictionary() extension method, that takes all object's tokens and puts in dictionary.
  - JsonToken.Add(JsonToken) method, that allows calling Append() from JsonToken type.
  - JsonToken.ValueCount virtual property to ease access to array one.
  - JsonToken.From(StreamReader) that returns unparsed token, just like string equivalent does.
  - JsonToken.Read(string) that returns parsed token, just like stream equivalent does.
  - ICommonToken interface, that both JsonToken and IToken implement (some of members, like indexer, explicitly), so that base operations could be performed interchangeably. Take into account that IToken needs to be casted.
  - IToken.Included() method, that does exactly what Include() does, but returns passed value of included (true by default). It can be used conveniently inside lambda expressions with predicates, so calls like Where(t => predicate).Select(t => t.Include()) could be shorten to Where(t => predicate ? t.Included() : false), or even Where(t => predicate && t.Included()).
  - JsonStream.DrawTokens() method that is designed purely to return tokens ready for operations outside of enumerable processing. In order to parse token it is advised to use Include() or Included() method. Alternatively, Count and ValueCount properties will cause immediate parsing, but may mess up caching strategy (ie. when Count is used for testing) - in such cases passing false to Include() or Included() after calling Count will cause token to be normally cached.
  - New compilation target: .NET Core 2.1, and AbstractString.ToSpan() method.
  
### Performance:
  - Reduced number of allocations for IToken deserialization under .NET Core 2.1
  - Reduced size of internal allocations made by CachingSerializer.
  - Reduced size of assembly around 2kb. Enumerator allocations done by JsonComposite.AsLazy() and JsonArray.Values() were reduced.
  - Array deserialization allocates less garbage.
  - JsonToken.Write() causes less allocations. Also its parformance for deeply nested structures improved marginally.
  - Object hashing got more attention (I was bout to delete this, coz it was purely useless). But before i did that, whole hashing was rewritten using different algorithm. Both hashing process and lookup time decreased around 20-30%, memory usage decreased ~15%. Also JsonObject instance size went down. So, according to my tests, with around 50 tokens inside object, both execution time of full lookup using linear search, and full lookup using hashing + hashing time were approximately equal, (in some cases even 40 was faster for hashing), and starting from ~60 keys hashing is consistently faster. So it seems that hashing has its worth. Altho it may speed up ie. deserialization in some cases, it isn't performed by default.
  - Deserialization using CachingDeserializer with object IToken should be noticeably faster, probably around 10-20%.
  - Regular parsing from StreamReader should be marginally faster. Also reduced number of allocations.

Next versions may target reading and writing of comments alongside json token hierarchy, default value serialization behaviour (0, false), enum serialization format (text / number) and further performance improvements. There are also things i'd try out but don't have specified desings in mind.


# 1.4.0:
### Changes to existing code:
  - JsonToken.From() returns JsonComposite instead of JsonToken.
  - Serializer.WriteTokensReadable()'s members and flag arguments has been removed,
  - Deserializer.LeftoverCount and alike structures have been removed,
  - JsonObject.Append(...) methods and JsonArray.Append(...) methods have been deprived of safeCopy and type arguments,
  - Extensions, namely ToJsonArray(), ToJsonObject(), AddRange() have been deprived of safeCopy and type arguments,
  - When deserializing model with constructor, public and private constructors could be used regardless of BindingFlags specified,
  - Deserializer no longer uses automatic code generation to speed up big array deserialization. This functionality has been moved to separate type.
  - ... anything I might have forgotten about.
  
### Fixes:
  - Support for null in JsonArray is extended for objects & arrays. Token's Remove(), SwapFor(), array's Append(), ToJsonArray() methods no longer leave array in state with corrupted indexing.
  - Serializer.WriteReadable or Produce (can't quite remember) would throw when serializing (for instance floats) on non-english culture environment. All serialization should now use invariant culture.
  - Removed faulty recursion in AddRange() extension methods that used params.
  - Fixed rare bug when null would serialize with quotes.
  - Fixed DateTime serialization to consistently use "s" (sortable - yyyy-MM-ddThh:mm:ss) formatting. Serialization format might be target feature for next release.
  
### New features:
  - JsonValue.ToUtcDateTime() conversion method (just DateTime.Parse really). ToBool(), ToLong(), ToDouble() and ToUtcDateTime() have been introduced for JsonToken as well.
  - JsonToken.Index read-only property was introduced. It contains index of token inside array. Could be used for sorting, when grouping of elements by type is disastrous, and original order is needed.
  - JsonArray.TokensWithNulls() extension method, that enumerates tokens, taking into consideration null positions (normal enumeration omits null tokens).
  - JsonObject.ToJsonObject() extension method, that creates object from dictionary output (IEnumerable<KeyValuePair<string,string>>). Key is assumed to be token's name, and Value is JsonToken value.
  - Deserializer.UseNameOrOmit property that allows to configure Serializer and Deserializer to look for [JsonName] and [JsonOmit] attributes. It may marginally improve performance if these attributes aren't used.
  - Time has finally came to make razor suitable for asp.net server environment (pun intended). Certain methods have been inspected in terms of thread safety, and those that could be changed, were rewritten in order to enable concurrency. Methods that are tested to be thread safe: 
	- JsonToken.From(string), 
	- Deserializer.Consume(string), 
	- Deserializer.Consume(JsonToken),
	- Serializer.ProduceTokens(T)
  The rule of thumb to guess thread safety is: if it uses StreamReader/Writer or char[] buffer, it may not be thread safe. 
  - New serialization types have been introduced in order to make serialization concurrent. 
  New types:
    - CachingSerializer,
	- CachingDeserializer,
	- CodeGenDeserializer.
  Each given class takes number of configuration parameters or is constructed via global Serializer or Deserializer configuration, and maintains its settings, regardless is global settings change. After initial configuration, their own settings should not change. Additionally, each instance may be reused to perform multiple serializations or deserializations, which should yield speed improvements for consecutive operations on same model type. 
  CachingSerializer class allows to specify own buffer instance, hence it supports wider range of thread-safe methods than static Serializer.
  CodeGenDeserializer performs dynamic code compilation, hence it should be cached for frequently used model types.
  - Added IToken.Convert(T) and JsonStream.StreamConverted() methods that support Deserialization. 
  - All serialization and deserialization methods previously available use new types under the hood. Some of method docs have been updated to reflect that.
  
### Performance: 
  - Reduced size of allocation for IEnumerator<JsonObject> or IEnumerator<JsonArray>.
  - New serialization classes can reduce number of allocations during whole process, depending on the input.
  - Increased performance when using CachingDeserializer on array with nested types.
  - Generated code is shorter and more efficient with value types.
  - Tests reported gigantic speed improvement when using new code (CachingSerializer). Test input array with 50 and 100 objects was serializing 65 - 75% faster. The tradeoff is that single object serialization may perform comparably or marginally worse.
  
This is one of biggest updates ever made to JsonRazor, in terms of edited code size. Altho set of tests has been extended, it is still possible for bugs to occur. I encourage You to post issues in public repo when any of these happen. Fuggit, even email me in emergent cases. Have a nice day.

1.3.1:
Changes to existing code:
  - JsonStream.StreamConverted<T>() generic class constraint was removed. Structs may now be used.
  - JsonArray value indexer will now throw ArgumentOutOfRangeException when index is greater or equal to value count.
  - JsonToken.GetFromPath() path first char that is type of token is now optional (but still correct to maintain compatibility) - with one exception - when path starts with array and immediately refers to next array, it can't be ommited. Example: [[{someProp
Fixes:
  - Deserializer wouldn't detect private constructors. 
  - It probably hadn't been mentioned, but sinse 1.3.0 serialization of system types got more attention. Namely, KeyValuePair<TKey, TValue> and ValueTuple<T...> were proved to work. 
  Sadly, Tuple<T...> class won't work, since it doesn't have default constructor and fields are readonly. If it wasn't the case, it most likely would. Structs have only constraint of fields being readonly.
  - Fixed rare error that could emerge when parsing via JsonStream.
  - Fixed another error that would make IToken corrupted. That would cause it to act as if arrays vanished (at least that was observed). There was also a possibility of exception being thrown when deserializing. 
  - Fixed rare error that could make JsonArray lose or omit first value in certain scenario (mainly when it was null).
  - Fixed error with JsonArray.GetFromPath, when it received path that doesn't end with property.
New features: 
  - I call it 'undefined type'. Let's say, You've got to store few possible classes in one place (object property, maybe array). But they differ in definition, so there's no base class or common field/prop surface is too small. It could be worked around by putting different prop types in object, and then check for presence of right one. But now comes a second solution. When field/property is of object type, a JsonObject or JsonArray that was parsed is put inside, so You may use it to selectively perform deserialization or data querying on it. List<object> or object[] could also be used as entrance type. When parsing happens with ITokens, still JsonTokens are put inside. Also serializer will use runtime type of object that's inside object field/prop.
Performance:  
  JsonStream caching strategy also got more attention. Investigation revelead that number of allocations could be reduced even further. In simple test scenario allocations of internal structure went from ~40 to 4. Also size of allocated memory was reduced and usage of structures was improved. In general, I'd expect JsonStream methods to consume less memory. It's hard to tell how this translates to speed, but lowering pressure on GC is also worth. Also some code paths have been marginally improved and old code was cleaned.

1.3.0:
Changes to existing code:
  I don't like to break compatibility but it somehow had to be done. So:
  -IToken.LoadTokens() behaviour was changed, since it was terribly stupid. It would stream tokens only once, when loaded.
  Now it streams them no matter in what loaded-state IToken is. Also all arguments were removed.
  -IToken.QueryTokens() still maintains only-one-enumeration behaviour (at least now it is documented), but optimizeLoad argument was removed. This is sole purpose of this method! Now, since LoadTokens works, it can replace it in other scenarios.
  -Assembly versioning abandons this retarded, 4-number notation. Now, 3-number notation may prevail.
  I was also gonna add default values to Serializer's methods ModelMembers args, but value Both is poorly choosen;
  changing it to Property would be inconsistent in the API, so i ended up givin' up. I will refrain from changing defaults, as it way have lots of unintended side effects.
Fixes:
  -JsonArray with only simple values could be in 'dirty' state even after being parsed.
  -JsonObject when parsed would throw exception on property names of 1 char.
  -JsonStream.StreamConverted() would throw on List<T>.
  -IToken.QueryTokens() would incorrectly manage loading when optimize load was enabled. Also QueryResult.Fail was handled incorrectly.
  -IToken.LoadTokens (and Count) would break state of object/array.
  -Quote-writing policy would be inconsistent. Now null, true and false serialize without quotes. Everything else with them.
  -There was something else but I totally forgot what it was, nvm it's fixed now.
New features:
  -Serialization and deserialization support for Nullable<T> - when in null state, it serializes to null, otherwise to content T of nullable.
  -Support for nulls in arrays! Previously when array was serialized with nulls, they would have been removed in deserialization. 
  Now this behaviour stays only with standard JsonToken parsing, but when deserializing such an array, nulls will stay at their original index. 
  This is sadly impossible for base parsing, since there's no type info.
  -JsonToken now exposes three new constants. This is really important to mention (really).
Performance:
  -Sadly, no noticeable change, probably. Some old code was dumped, so assembly size went down around 1KB. 
  IToken.Count was optimised a bit.
Docs:
  Some of documentation headers were edited (in placed when they obviously stated some gibberish) and new were added where appriopriate, altho there're still blanks.
New year, new better version, etc etc. I'm really sorry that it took that long for these changes, i've just been busy and doing other important stuff. Now things will change a bit (I hope).
Next version release will most likely bring better IToken query method, maybe with overloads for de/serializing that take Type, instead of being generic.

1.2.2.8:
Added .Net Standard 2.0 and .Net Core 2.0 assemblies.
Few constansts went public. Also added some lacking documentation.

1.2.2.7:
Fixes:
  -DateTime and other IFormattable will always be serialized with quotes. Serialization and Deserialization should use InvariantCulture for culture specific tasks.
Writing tokens can perform marginally faster.

1.2.2.6:
More speeeed!!!!oneone!
Deserialization went one more time through performance investigation, which showed gain in speed up to 60% with huge array or objects. With 10x smaller array, speed gain was about 30%. This improvement is accesible both from token and IToken (linq).
New features:
  -JsonStream.StreamConverted method, which replaces tedious StreamFile.Select(t => t.Convert()), and brings these improvements for 0 tier of array members.

1.2.2.5:
Changes to existing code:
  Since [JsonInfo] was source of problems and logic holes i decided to remove its name mapping function. It's now valid only on type declaration, and [JsonName] is meant for name mapping.
Fixes:
  -Exceptions that could be thrown in certain JsonName/Info configurations,
  -Rare exception that could be thrown when field/property is null,
  -Serialization no longer produces compiled generated backing fields from auto properties in certain cases.
New features:
  -Serializer.OmitNullTokens property dictates if field/property with null value will be processed. True by default.
Deserializer speed improvement when processing arrays of object. With large input, speed gain up to 15%.

1.2.2.4:
Changes to existing code:
  -JsonNameAttribute replaces JsonInfoAttribute's name mapping (JsonInfo is now intended for stronger customizations).
  -JsonObject.Append() name argument is by default inferred from token.
Fixes:
  -Critical bug in JsonObject/Array copying mechanism (both constructors) producing undefined errors when these outputs are used.
  -Other minor fixes.
New features:
  -Control your IEnumerable serialization - place JsonIgnoreEnumerableAttribute on types that do implement these, but are intended to be serialized as object would.
  -Serializer's WriteReadable & WriteTokensReadable methods that writes using ToString() formatting. Generally, ProduceTokens()/ToString() would yield the same effect, but said methods are preoptimised and win in performance when content grows.
  -AddRange() JsonObject/Array extension methods that simplify usage of Append() methods.
  -Deserializer's GetRemainingToken returns token that didn't deserialize properly. it's quite obvious it works only when root token is object.
Besides that, ToString() performance improvement and reduced memory usage. Slight Hash() performance improvement.
  
  

1.2.2.1:
Fixes:
  -Critical bug causing exception in Serializer.ProduceTokens() (dunno how this got past thru tests).
  -File parsing error when 'null' was w/o quotations.
  -SwapFor() would sometimes work like Remove().
  -Append() is more secure and less-problem prone (actually no problems detected now, but who knows all possible cases?).
  -Possible problems with First... property may now be gone (actually no problems detected either, but code looked wonky somehow).
  -AsLazy() possible problems fixed.
  -JsonToken extension methods may now not throw NullReference exception.
  -Replaced [CanBeNull] with [NotNull] in JsonObject.Append().
Slight performance improvemens, mainly to reduce garbage generation while using Append() and methods that don't like it,as well as reduced code size.
PS.  This is very important update, as it contains major fixes.
PS.  I'm really ashamed these errors were ever published. Some of them were introduced in small portions in concurrent updates, making hard to detect. I hope they weren't a huge problem, influencing development of your applications.

1.2.2.0:
Changes to existing code:
  -JsonMapperAttribute no longer exist. It was  replaced by JsonInfoAttribute, which in fact gives more potencial in adjusting de/serialization.
  -JsonConstructedAttribute was also replaced by JsonInfo. Luckily enough, this won't affect official release.
  -MemberTypes enumeration was replaced by ModelMembers enumeration, designed specifically for de/serialization.
  -serializers now include private & protected members by default.
New features:
  -JsonInfoAttribute supports both selecting custom constructor and setting model member types, BindingFlags to select public or private members, and custom json name. Works both for serialization and deserialization and can be placed on type declaration, as well as on member declaration, overriding type attribute.
  -JsonOmitAttribute allows to exclude field/property from being de/serialized. It is adviced to not place omit & info attributes on the same member.
  -Linq.IToken has new Convert<>() method, automatically deserializing token into given type.
Slight deserialization & Linq performance improvements.

1.2.1.0:
Changes to existing code:
  -JsonObject.Append() and JsonArray.Append() no longer require to not have parent - I believe dear users won't break this somehow.
Fixes:
  -Error causing string parsing to fail at the end, if minified.
  -Weird bug causing children count go down after appending tokens/values.
New features:
  Im pround to announce that serialization features finally are a lot more reliable and usable. -Both serialization and deserialization now support structs the same way they do for classes.
  -It's possible to generate type instance during deserialization with choosen constructor with particular argument list, via JsonConstructedAttribute. Type instance may be further processed to match remaining fields/properties or returned at this point.
  -Serialization got new method to generate usual json token hierarchy instead serializing to text. Useful, when data model is needed to be extended or manipulated before saving.
  Besides serialization, Linq.AbstractToken now implements IComparable<string> and allows to compare ignoring case.
No visible performance improvements.

1.2.0.0:
Changes to existing code:
    -(string)jsonToken and (string)value are no longer necessary, since casts are implicit for convenience. Beware, as it may now require to call ToString() to gain json representation.
    -appending tokens to object/array no longer require them to be parsed.
    -It's now possible to parse file and string simultaneusly, although i can't guarantee everything will go alright.
Fixed issues:
    -JetBrains.Annotations kept wanishing from API due to fucked  up configuration. This solution finally fixes it, i hope.
    -JsonObject [] indexer for properties would sometimes omit first token.
    -JsonObject.FirstObject wouldn't load object if it hadn't had one loaded.
    -seems like string parsed tokens wouldn't know whether they had quotes with value or not.
    -fixed really rare error causing parsing to fail. Parsing logic was reworked a little, but no errors found.
New features:
    Parsing file data now has an alternative. Features sit under Linq namespace and are designed to provide you portion of selected,
        quered data in extremely performant way. Although actual speed depends on structure of file, choosen query method and extracted data size,
        new way will most likely be faster than conventional way, and have greatly reduced memory usage. This feature is likely to be updated with newer query methods, etc.
Slight performance improvements. 


1.1.0.10:
Fixes:
  -rather rare vulnerability causing parsing to fail after parsing children,
  -GetFromPath() would return only properties, now fixed.
Slight API and performance improvements.

1.1.0.8:
Fixes:
  -parser now accepts escaped chars (ie. quotation marks).
New features:
  -JsonReader can be created once, and then used any time for different streams (saves buffer creation).
Improved parsing (string/file) speed. Getting really close to absolute performance.

1.1.0.7:
Changes to existing code: 
    -(IEnumerable<JsonToken>)obj no longer enumerates properties only. To preserve this functionality, use Properties() extension method instead.
New features: 
    -Remove/Replace methods for tokens,
    -changing token's name,
    -some methods to reduce typing, ie. Read, Write, ToJsonObject, ToJsonArray, Properties, Tokens, Arrays, Objects, Values, all working on JsonToken,
    -some methods, accepting either object or array, now work for tokens, ie. Parse(),
    -minor interface improvements.
Improved performance (ie. reading, ToString())